with (import <nixpkgs> {});
let
  gems = bundlerEnv {
    name = "pygotham2018";
    inherit ruby_2_4;
    gemdir = ./.;
  };
in stdenv.mkDerivation {
  name = "pygotham2018";
  buildInputs = [gems ruby_2_4];
}
