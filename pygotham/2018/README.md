# PyGotham 2018

This is a nix file that can be used to install the requirements for a
Jekyll-powered website. In order to use it you must first generate a
`gemset.nix` file. This can be done by installing
[bundix](https://github.com/manveru/bundix) and running

    bundix -l