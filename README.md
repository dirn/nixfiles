# Nix Files

This repository contains a set of `default.nix` files that I use.

I got tired of recreating them every time I try to use `nix-env` on a new
machine. They aren't intended to be guides and won't (intentionally) reflect
that state of the art. The simply served my needs at the time I wrote them.
